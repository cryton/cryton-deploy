# Cryton deployment with Ansible
This project is used for deploying the Cryton toolset using Ansible.  

Cryton toolset is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that 
**only the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

- Make sure you run Ansible using Python3 (`ansible_python_interpreter: /usr/bin/python3`).  
- Supposedly there is no need for `gather_facts`.  
- Run roles using sudo (become).  
- For the best experience specify `cryton_COMPONENT_version` where *COMPONENT* is depending on the role (core, cli, worker, modules)
and select the latest version (**the master branch is not stable**).  
- To update the default variables stored in .env files use `cryton_COMPONENT_environment` where *COMPONENT* is depending on the role (core, cli, worker).  
- Values for each role can be found in `cryton-deploy/roles/ROLE/defaults/main.yml`.  
- Once you update cryton_COMPONENT_environment, **make sure you've updated all the variables**.

## Roles

### deploy-core
Install prerequisites, dependencies (RabbitMQ, Postgres, and PgBouncer), and Core using Docker Compose.  
Core's REST API is by default served on port 8000.

Override environment variables as specified in the [settings](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/2022.2/starting-point/core/#settings). 
In the Ansible playbook use the following:
```yaml
  - role: deploy-core
    cryton_core_environment:
      VARIABLE_TO_OVERRIDE: new_value
      ... 
```

For all available role variables, check `cryton-deploy/roles/deploy-core/defaults/main.yml`.

To create, update, and load the Docker configuration saved in /etc/docker/daemon.json, set `update_docker_daemon_configuration: yes`,
and use `docker_daemon_configuration` dictionary to create the configuration.  
Example and default:
```
docker_daemon_configuration:
  mtu: 1442
```

### deploy-worker (with modules)
Install prerequisites and Worker with modules using pipx.  
Start the Worker afterward in the background (you have to check for errors manually in the `{{ cryton_worker_output_file }}`).

To start msfrpcd in the background use `start_msfrpcd: yes`.  
Set `cryton_cli_runas_user` to the correct user for whom will the Worker be installed.

Optionally, Worker can be installed in a mode fitting for development purposes. To enable this mode, set ``development: True`` variable for Ansible. 
This will install and run the Worker using poetry.

Override environment variables as specified in the [settings](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/2022.2/starting-point/worker/#settings). 
In the Ansible playbook use the following:
```yaml
  - role: deploy-worker
    cryton_worker_environment:
      VARIABLE_TO_OVERRIDE: new_value
      ... 
```

For all available role variables, check `cryton-deploy/roles/deploy-worker/defaults/main.yml`.

For running the Ansible playbook, community.general module is needed. Install it by ``ansible-galaxy collection install community.general``.

### deploy-cli
Install prerequisites, dependencies, and CLI in `~/.local/bin/cryton-cli` using pipx, register it to PATH, and export .env vars into `~/.local/cryton-cli/.env`.

Set `cryton_cli_runas_user` to the correct user for whom will the Worker be installed.

Override environment variables as specified in the [settings](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/2022.2/starting-point/cli/#settings). 
In the Ansible playbook use the following:
```yaml
  - role: deploy-cli
    cryton_cli_environment:
      VARIABLE_TO_OVERRIDE: new_value
      ... 
```

For all available role variables, check `cryton-deploy/roles/deploy-cli/defaults/main.yml`.

### register-worker
Register Worker in Core using CLI.

Specify `cryton_worker_name`, `cryton_worker_description`, and `cryton_cli_runas_user` to the correct user with access to the CLI.

Override environment variables as specified in the [settings](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/2022.2/starting-point/cli/#settings). 
In the Ansible playbook use the following:
```yaml
  - role: register-worker
    cryton_cli_environment:
      VARIABLE_TO_OVERRIDE: new_value
      ... 
```

For all available role variables, check `cryton-deploy/roles/register-worker/defaults/main.yml`.

### deploy-frontend
Install prerequisites and frontend for Cryton Core API using Docker Compose. The frontend is by default served on port 8080.

**!This role requires the host to have at least 2048 MB RAM and 2 CPU cores (tested with AMD Ryzen 5 5600x) otherwise the Frontend installation might fail.!**

Override environment variables as specified in the [settings](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/2022.2/starting-point/frontend/#settings). 
In the Ansible playbook use the following:
```yaml
  - role: deploy-frontend
    cryton_frontend_environment:
      VARIABLE_TO_OVERRIDE: new_value
      ... 
```

For all available role variables, check `cryton-deploy/roles/deploy-frontend/defaults/main.yml`.

To create, update, and load the Docker configuration saved in /etc/docker/daemon.json, set `update_docker_daemon_configuration: yes`,
and use `docker_daemon_configuration` dictionary to create the configuration.  
Example and default:
```
docker_daemon_configuration:
  mtu: 1442
```

## Examples

### Deploy Core
```yaml
- name: Deploy Core
  hosts: c2-server
  become: yes
  roles:
    - role: deploy-core
```

### Deploy Worker (with modules)
```yaml
- name: Deploy Worker with modules
  hosts: attacker
  become: yes
  roles:
    - role: deploy-worker
      cryton_worker_runas_user: my-user
      cryton_worker_environment:
        CRYTON_WORKER_MODULES_DIR: "{{ cryton_modules_directory }}/modules"
        CRYTON_WORKER_RABBIT_HOST: 127.0.0.1
        CRYTON_WORKER_NAME: unique-name
```

### Deploy CLI
```yaml
- name: Deploy CLI
  hosts: client
  become: yes
  roles:
    - role: deploy-cli
      cryton_cli_runas_user: my-user
      cryton_cli_environment:
        CRYTON_CLI_API_HOST: 127.0.0.1
```

### Register Worker
```yaml
- name: Register Worker
  hosts: client
  roles:
    - role: register-worker
      cryton_cli_runas_user: my-user
      cryton_worker_name: unique-name
      cryton_worker_description: custom Worker description
      cryton_cli_environment:
        CRYTON_CLI_API_HOST: 127.0.0.1
```

### Deploy CLI and register new Worker
```yaml
- name: Deploy CLI and register Worker
  hosts: client
  become: yes
  vars:
    cryton_cli_runas_user: my-user
    cryton_cli_environment:
        CRYTON_CLI_API_HOST: 127.0.0.1
  roles:
    - role: deploy-cli
    - role: register-worker
      cryton_worker_name: unique-name
      cryton_worker_description: custom Worker description
```

### Deploy frontend
```yaml
- name: Deploy frontend
  hosts: client
  become: yes
  roles:
    - role: deploy-frontend
      cryton_frontend_environment:
        crytonRESTApiHost: 127.0.0.1
```
